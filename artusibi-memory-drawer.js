/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var memoryDrawerType = function(context) {
        var drawer = this;
        drawer.context = context;
        drawer.kitchen = context.kitchen;
    };

    /**
     * @options:
     *   @drawerIRI the IRI of the drawer.
     */
    memoryDrawerType.prototype.init = function() {
        var dr = $.Deferred();
        var drawer = this;

        /**
         * Resources.
         */
        drawer.resources = {};

        /**
         * Registry indexes.
         */
        drawer.indexes = {};

        /**
         * Code of the recipes.
         */
        drawer.recipesCode = {};

        drawer.recipeBookCache = {};

        return dr.promise();
    };

    memoryDrawerType.prototype.getIcon32Url = function() {
        return "http://icons.iconarchive.com/icons/tpdkdesign.net/refresh-cl/256/System-Memory-icon.png";
    };

    memoryDrawerType.prototype.getServiceRecipes = function(options) {
        var drawer = this;
        var dr = $.Deferred();

        dr.resolve([]);

        return dr.promise();
    };

    memoryDrawerType.prototype.ssoupRecipe = function(recipeIRI, recipeCode) {
        this.recipesCode[recipeIRI] = recipeCode;
    };

    /**
     * Artusi implementation of SSOUP [drawer.knows](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.knows)
     *
     * @options:
     * 	@resourceIRI the IRI of the resource to check the existence
     * @return true if the registry contains the resource with the given IRI as a subject of at least one triple, false otherwise.
     */
    memoryDrawerType.prototype.knows = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getResource(options).done(function(resource) {
            if (resource) {
                dr.resolve(true);
            } else {
                dr.resolve(false);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.get)
     *
     * @options:
     *   @resourceIRI the IRI of the resource to retrieve
     *   @postProcess how to post process the resource: none, eval, jQuery; defaults to eval
     * @return the resource.
     */
    memoryDrawerType.prototype.getResource = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var resourceIRI = drawer.applyContext(options.resourceIRI);
        var namespace = tools.getNamespace(resourceIRI);
        if (!namespace) {
            namespace = "ssoup";
        }

        console.info("Retrieving document " + resourceIRI + " in collection " + namespace);
        dr.resolve(drawer.resources[namespace + ":" + resourceIRI]);

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.put](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.put)
     *
     * @options:
     *    @resourceIRI the IRI of the resource to save
     *    @data the data to save to the register
     */
    memoryDrawerType.prototype.put = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        // resourceIRI can be either absolute or relative to the drawer path
        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        //var path = drawer.conf.path + '/' + resourceIRI;

        resourceIRI = drawer.applyContext(options.resourceIRI);
        var namespace = tools.getNamespace(resourceIRI);
        if (!namespace) {
            namespace = "ssoup";
        }

        console.info("Updating resource " + namespace + ", document @id=" + resourceIRI);
        drawer.resources[namespace + ":" + resourceIRI] = options.data;

        return dr.promise();
    };

    /**
     * Retrieves info about the indexes in the drawer.
     *
     * @options
     */
    memoryDrawerType.prototype.getIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Build all the indexes by scanning all the resources in the drawer.
     *
     * @options
     */
    memoryDrawerType.prototype.buildIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    }

    /**
     * Build an index by scanning all the resources in the drawer.
     *
     * @options
     *	@indexInfo the info of the index to rebuild.
     */
    memoryDrawerType.prototype.buildIndex = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Update an index by rescanning all the resources in the specified tree.
     * Blob fetching can be parallelized.
     *
     * @options
     *	@indexInfo the info structure about the index to build
     *	@index the index to update.
     *	@tree the tree to scan.
     * @return an indexing structure for the tree
     */
    memoryDrawerType.prototype.buildIndexForTree = function(options) {
        var deferreds = [];
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    return memoryDrawerType;
});
